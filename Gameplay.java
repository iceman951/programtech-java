import java.io.*;
import java.util.*;

public class Gameplay {
    public static void main(String  agr[]) {
        Monster monster = new Monster();
        Scanner reader = new Scanner(System.in);

        System.out.println("Welcome to New World!");
        System.out.print("Enter Your name: ");
        String name = reader.nextLine();
        Novice novice = new Novice(name);

        System.out.print("<<< Choose Your Job >>>\n+1.Swordman\n+2.Merchant\nYou choose: ");
        int chooseJob = reader.nextInt();
        while(true) {
            if (chooseJob ==1) {
                novice = new Swordman(novice);
                System.out.println("You are Swordman!!!");
                break;
            }
            else if (chooseJob == 2) {
                novice = new Merchant(novice);
                System.out.println("Your are Merchant!!!");
                break;
            }
        }
        Interface playerInterface = new Interface();
        
        while(true){ //main

            playerInterface.main(novice); //show main interface
            System.out.print("Choose Your choice: ");
            int mainChoice = reader.nextInt(); //choose mainchoice

            if(mainChoice == 1){
                while(true){

                    playerInterface.store(); //show store interface
                    System.out.print("Choose Your choice: ");
                    int choiceForBuy = reader.nextInt(); //choose choiceforbuy

                    if(choiceForBuy == 1) { //buypotion
                        if (novice.bag.checkFullBag() >= novice.bag.getBagCapacity()){
                            System.out.println("Bag is Full");
                        }
                        else {
                        novice.bag.putPotionInBag();
                        novice.buyItem(20);
                        }
                    }
                    else if(choiceForBuy == 2) { //buymaxpotion
                        if (novice.bag.checkFullBag() >= novice.bag.getBagCapacity()){
                            System.out.println("Bag is Full");
                        }
                        else {
                        novice.bag.putMaxPotionInBag();
                        novice.buyItem(100);
                        }
                    }
                    else if(choiceForBuy == 3) { //buykunai
                        if (novice.bag.checkFullBag() >= novice.bag.getBagCapacity()){
                            System.out.println("Bag is Full");
                        }
                        else {
                        novice.bag.putKunaiInBag();
                        novice.buyItem(100);
                        }
                    }
                    else if(choiceForBuy == 4) {
                        break;
                    }
                }
            }

            else if (mainChoice == 2) {
                novice.bag.showItems();
            }
            else if (mainChoice == 3) {
                playerInterface.hunt();
                System.out.print("Choose Your choice: ");
                int choiceForHunt = reader.nextInt();
                if(choiceForHunt == 1){
                    monster.setGreenSlime();
                }
                else if(choiceForHunt == 2){
                    monster.setRedSlime();
                }
                else if(choiceForHunt == 3){
                    break;
                }

                while (monster.getHp()>0){
                    playerInterface.whileHunt();
                    System.out.print("Choose Your choice: ");
                    int choiceWhileHunt = reader.nextInt();
                    if (choiceWhileHunt == 1) { //Normail Attack
                        novice.takeDamage(monster.getAtk());
                        monster.takeDamage(novice.getAtk());
                        System.out.println("NoviceHp: " + novice.getHp() + "/" + novice.getMaxHp());
                        System.out.println("MonsterHp: " + monster.getHp());
                    }
                    else if (choiceWhileHunt == 2) { //Use Item
                        novice.bag.showItems();
                        System.out.println("Choose Item(Enter No. of Item): ");
                        int choiceUseItem = reader.nextInt();
                        if (novice.bag.items.get(choiceUseItem-1).name.equals("Kunai")) {
                            novice.takeDamage(monster.getAtk());
                            monster.takeDamage(novice.bag.items.get(choiceUseItem-1).damage);
                            novice.bag.items.get(choiceUseItem-1).print();
                            novice.bag.useItem(choiceUseItem);
                            System.out.println("NoviceHp: " + novice.getHp() + "/" + novice.getMaxHp());
                            System.out.println("MonsterHp: " + monster.getHp());
                        }
                        else if (novice.bag.items.get(choiceUseItem-1).name.equals("Potion")) {
                            novice.takeDamage(monster.getAtk());
                            novice.usePotion(novice.bag.items.get(choiceUseItem-1).heal);
                            novice.bag.items.get(choiceUseItem-1).print();
                            novice.bag.useItem(choiceUseItem);
                            System.out.println("NoviceHp: " + novice.getHp() + "/" + novice.getMaxHp());
                            System.out.println("MonsterHp: " + monster.getHp());
                        }
                        else if (novice.bag.items.get(choiceUseItem-1).name.equals("MaxPotion")) {
                            novice.takeDamage(monster.getAtk());
                            novice.usePotion(novice.bag.items.get(choiceUseItem-1).heal);
                            novice.bag.items.get(choiceUseItem-1).print();
                            novice.bag.useItem(choiceUseItem);
                            System.out.println("NoviceHp: " + novice.getHp() + "/" + novice.getMaxHp());
                            System.out.println("MonsterHp: " + monster.getHp());
                        }
                        
                    }
                    else if (choiceWhileHunt == 3) {
                        novice.takeDamage(monster.getAtk());
                        monster.takeDamage(novice.getAtk()+novice.useSkill());
                    }

                    if(novice.getHp() <= 0){
                        System.out.println("You Died Pleas try again");
                        break;
                    }
                }
                if (novice.getHp() >=1 ){
                    novice.expGain(monster.getExp());
                    novice.monsterMoneyDrop(monster.getMoney());
                }
                else if(novice.getHp()<0){
                    System.out.println("Reborn");
                    novice.reborn();
                }
                novice.levelUp(novice.getExp(), novice.getLevel());
            }

            else if(mainChoice == 4) {
                break;
            }
        }
    }
}
