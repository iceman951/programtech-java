import java.io.*;
import java.util.*;

public class Swordman extends Novice {
    public Swordman(Novice novice){
       super(novice.getNoviceName());
       setJob("Swordman", novice.getHp(), novice.getMaxHp(), novice.getLevel(), novice.getAtk(), novice.bag);
    }

    public int useSkill() {
        if(getHp()>6 ) {
            System.out.println("<<< Use Blood Rage Skill >>>");
            takeDamage(5);
            return getAtk()+10;
        }else {
            System.out.println("<<< Your HP must greater than 6 >>>");
            return 0;
        }
    }

}