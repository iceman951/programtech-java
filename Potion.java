import java.io.*;
import java.util.*;

public class Potion extends Item {
    public Potion(String name, int damage, int heal){
        super(name, damage, heal);
    }
    public void print() {
        System.out.println("\n" + name + " heal " + heal + " HP");
    }
}