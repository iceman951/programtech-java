import java.io.*;
import java.util.*;

public class Merchant extends Novice {
    public Merchant(Novice novice){
       super(novice.getNoviceName());
       setJob("Merchant", novice.getHp(), novice.getMaxHp(), novice.getLevel(), novice.getAtk(), novice.bag);
    }

    public int useSkill() {
        if(getMoney()>=20 ) {
            System.out.println("<<< Use Money Rage Skill >>>");
            useMoney(20);
            return getAtk()+20;
        }else {
            System.out.println("<<< Your Money must greater than 20 >>>");
            return 0;
        }
    }
}