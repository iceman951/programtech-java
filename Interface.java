import java.io.*;
import java.util.*;

public class Interface{
    public void main(Novice novice) {
        System.out.println("=======================");
        System.out.println("+ Name: " + novice.getNoviceName());
        System.out.println("+ Job: "+ novice.getJob());
        System.out.println("+ HP: " + novice.getHp() + "/" + novice.getMaxHp());
        System.out.println("+ AttackPoint: " + novice.getAtk());
        System.out.println("+ Level: " + novice.getLevel());
        System.out.println("+ EXP: " + novice.getExp());
        System.out.println("+ Money: " + novice.getMoney());
        System.out.println("=======================\n");
        
        System.out.println("<<< =========****========= >>>");
        System.out.println("+ 1. Store");
        System.out.println("+ 2. Bag");
        System.out.println("+ 3. Let's Hunt (Coming Soon)");
        System.out.println("+ 4. Exit");
        System.out.println("<<< ====================== >>>\n");
    }
    public void store() {
        System.out.println("*************************");
        System.out.println("");
        System.out.println("Welcome to Store, Choose Your Item to Buy");
        System.out.println("+ 1. Potion");
        System.out.println("+ 2. MaxPotion");
        System.out.println("+ 3. Kunai");
        System.out.println("+ 4. back");
        System.out.println("");
        System.out.println("*************************");
    }
    public void hunt(){
        System.out.println("=======================");
        System.out.println("Let's Hunt Monsters, Choose a Monster");
        System.out.println("+ 1. Green Slime");
        System.out.println("+ 2. Red Slime");
        System.out.println("+ 3. back");
        System.out.println("=======================\n");
    }
    public void whileHunt() {
        System.out.println("=======================");
        System.out.println("+ 1. Normal Attack");
        System.out.println("+ 2. Use Item");
        System.out.println("+ 3. Skill Attack");
        System.out.println("=======================");
    }
}