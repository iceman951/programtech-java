import java.io.*;
import java.util.*;

public class Novice {
    private String noviceName;
    private String job;
    private int hp;
    private int maxHp;
    private int level;
    private int atk;
    private int exp;
    private int money;
    Bag bag;

    public Novice(String name){
       noviceName = name;
       job = "Novice";
       maxHp = 40;
       hp = maxHp;
       level = 1;
       atk = 5;
       exp = 0;
       money = 500;
       bag = new Bag();
    }
    public void setJob(String _job, int _hp, int _maxHp, int _level, int _atk, Bag _bag) {
        job = _job;
        hp = _hp;
        maxHp = _maxHp;
        level = _level;
        atk = _atk;
        bag = _bag;
    }
    public String getJob() {
        return job;
    }
    public int getHp() {
        return hp;
    }
    public int getMaxHp() {
        return maxHp;
    }
    public int getLevel() {
        return level;
    }
    public int getAtk() {
        return atk;
    }
    public int getExp() {
        return exp;
    }
    public int getMoney() {
        return money;
    }
    public void useMoney(int cost) {
        money -= cost;
    }
    public int useSkill() {
        int skillDamage=0;
        return skillDamage;
    }

    public void buyItem(int cost) {
        if (money < cost){
            System.out.println("\nNot Enought Money!!!\n");
        }
        else    {
            money = money - cost;
            System.out.println("\nThaks For Purchased!!!\n");
        }
    }
    public void setNoviceName(String name) {
        noviceName = name;
    }
    public String getNoviceName() {
        return noviceName;
    }
    public void expGain(int expgain) {
        exp = exp + expgain;
    }
    public void monsterMoneyDrop(int dropMoney) {
        money = money + dropMoney;
    }
    public void takeDamage(int damage) {
        hp = hp - damage;
    }
    public void reborn() {
        hp = maxHp;
        exp = 0;
        money = money/2;
    }
    public void usePotion(int healPoint) {
        hp = hp + healPoint;
        if(hp>maxHp) hp = maxHp;
    }
    public void levelUp(int lvlupExp, int lvlupLevel) {
        if (lvlupExp > lvlupLevel*50){
        level++;
        atk++;
        exp = 0;
        maxHp = maxHp+10;
        hp = maxHp;
        System.out.println("Congrat!! now your Level is " + level);
        }
    }
}